# Portfolio HarleySG

Este es desarrollo de mi portafolio web usando el framework tipo `frankenstein`, que es la personalización de:
* [Materialize](https://github.com/Dogfalo/materialize) y
* [Ed Grid](https://github.com/escueladigital/ED-GRID).

### Agradecimientos
> El desarrollo de este framework fue motivado tras el constante uso de librerias para desarrollo front end y la poca compatibilidad de estos entre si asi como el deseo de establecer, luego de varios años de aprendizaje y experimientación con librerias, un ejercicio propio, modular, personalizable para mis proyectos. - Harley SG